public void misc_MapInfo() {
	GetCurrentMap(gC_MapName, 128);
}

public void misc_ClearSpawns() {
	gA_SpawnPoints.Clear();

	for (int i = 0; i < 2; i++) {
		gI_SpawnPoints[i] = 0;
	}
}

public void misc_ClearItems() {
	gA_Items.Clear();

	gI_Items = 0;
}

public void misc_FormatTeam(int I_Team, char[] C_buffer, int I_MaxLength) {
	switch (I_Team) {
		case 2: {
			Format(C_buffer, I_MaxLength, "T");
		} case 3: {
			Format(C_buffer, I_MaxLength, "CT");
		}
	}
}

public void misc_FormatItem(int I_Item, char[] C_buffer, int I_MaxLength) {
	switch (I_Item) {
		case 0: {
			Format(C_buffer, I_MaxLength, "Smoke");
		} case 1:{
			Format(C_buffer, I_MaxLength, "Molotov");
		}
	}
}
