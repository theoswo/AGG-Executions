public void sql_Connect() {
	char[] C_Error = new char[512];

	gD_Main = SQL_Connect("Executions", true, C_Error, 512)

	if (gD_Main == INVALID_HANDLE) {
		CloseHandle(gD_Main);

		SetFailState("[SQL] Error to connect to Executions - %s", C_Error);
		return;
	}

	sql_CheckTables();
}

public void sql_CheckTables() {
	bool B_Failed;

	SQL_LockDatabase(gD_Main);

	if (!SQL_FastQuery(gD_Main, "SELECT * FROM `ex_SpawnPoints` LIMIT 1;")) {
		B_Failed = true;
	}

	SQL_UnlockDatabase(gD_Main);

	if (B_Failed) {
		sql_CreateTables();
	}
}

public void sql_CreateTables() {
	char[] C_buffer = new char[512];
	Transaction T_Tables = SQL_CreateTransaction();

	Format(C_buffer, 512, "CREATE TABLE IF NOT EXISTS `ex_SpawnPoints` (mapname VARCHAR(64) NOT NULL, id INT(3) NOT NULL, priority INT(3) NOT NULL, team INT(3) NOT NULL, pos_x FLOAT NOT NULL, pos_y FLOAT NOT NULL, pos_z FLOAT NOT NULL, eye_x FLOAT NOT NULL, eye_y FLOAT NOT NULL, eye_z FLOAT NOT NULL, PRIMARY KEY (mapname, id));");
	T_Tables.AddQuery(C_buffer);

	Format(C_buffer, 512, "CREATE TABLE IF NOT EXISTS `ex_Items` (mapname VARCHAR(64) NOT NULL, id INT(3) NOT NULL, itemid INT(3) NOT NULL, pos_x FLOAT NOT NULL, pos_y FLOAT NOT NULL, pos_z FLOAT NOT NULL, PRIMARY KEY (mapname, id));");
	T_Tables.AddQuery(C_buffer);

	SQL_ExecuteTransaction(gD_Main, T_Tables, sqlTryCreateTablesSuccess, sqlTryCreateTablesError, _, DBPrio_High);
}

public void sqlTryCreateTablesSuccess(Database D_Database, any D_Data, int I_Queries, Handle[] H_Results, any[] D_QueryData) { }

public void sqlTryCreateTablesError(Database D_Database, any D_Data, int I_Queries, char[] C_Error, int I_FailIndex, any[] D_QueryData) {
	LogError("%s (TryCreateTables) - %s", SQL_PREFIX, C_Error);
}

public void sql_LoadSpawnPoints() {
	char[] C_buffer = new char[512];

	Format(C_buffer, 512, "SELECT * FROM `ex_SpawnPoints` WHERE mapname = '%s' ORDER BY id ASC", gC_MapName);
	SQL_TQuery(gD_Main, sqlLoadSpawnPointsCallback, C_buffer, _, DBPrio_High);
}

public void sqlLoadSpawnPointsCallback(Handle H_Owner, Handle H_Hndl, char[] C_Error, any A_Data) {
	if (H_Hndl == null) {
		LogError("%s (LoadSpawnPoints) - %s", SQL_PREFIX, C_Error);
		return;
	}

	int I_Total, I_Team;

	misc_ClearSpawns();

	while (SQL_FetchRow(H_Hndl)) {
		I_Team = SQL_FetchInt(H_Hndl, 3);

		for (int i = 0; i < 2; i++) {
			I_Total += gI_SpawnPoints[i];
		}

		gA_SpawnPoints.Resize(I_Total + 1);

		gA_SpawnPoints.Set(I_Total, SQL_FetchInt(H_Hndl, 1), 0);
		gA_SpawnPoints.Set(I_Total, SQL_FetchInt(H_Hndl, 2), 1);
		gA_SpawnPoints.Set(I_Total, I_Team, 2);

		for (int i = 0; i < 3; i++) {
			gA_SpawnPoints.Set(I_Total, SQL_FetchFloat(H_Hndl, i + 4), i + 3);
			gA_SpawnPoints.Set(I_Total, SQL_FetchFloat(H_Hndl, i + 7), i + 6);
		}

		if (I_Team == 2) {
			gI_SpawnPoints[0]++;
		} else if (I_Team == 3) {
			gI_SpawnPoints[1]++;
		}
	}
}

public void sql_TryInsertSpawn(int I_Client, int I_Id, int I_Priority, int I_Team, float F_Pos[3], float F_Eye[3]) {
	char[] C_buffer = new char[512];
	DataPack D_Pack = new DataPack();
	int I_UserId;

	I_UserId = GetClientUserId(I_Client);

	D_Pack.WriteCell(I_UserId);
	D_Pack.WriteCell(I_Id);
	D_Pack.WriteCell(I_Priority);
	D_Pack.WriteCell(I_Team);

	for (int i = 0; i < 3; i++) {
		D_Pack.WriteFloat(F_Pos[i]);
	}

	for (int i = 0; i < 3; i++) {
		D_Pack.WriteFloat(F_Eye[i]);
	}

	Format(C_buffer, 512, "SELECT * FROM `ex_SpawnPoints` WHERE id = '%i' AND mapname = '%s'", I_Id, gC_MapName);
	SQL_TQuery(gD_Main, sqlTryInsertSpawnCallback, C_buffer, D_Pack, DBPrio_Normal);
}

public void sqlTryInsertSpawnCallback(Handle H_Owner, Handle H_Hndl, char[] C_Error, DataPack D_Pack) {
	if (H_Hndl == null) {
		LogError("%s (TryInsertSpawn) - %s", SQL_PREFIX, C_Error);
		return;
	}

	char[] C_buffer = new char[512];
	char[] C_MapName = new char[64];
	int I_UserId, I_Id, I_Priority, I_Team;
	float F_Pos[3], F_Eye[3];

	GetCurrentMap(C_MapName, 64);

	D_Pack.Reset();

	I_UserId = D_Pack.ReadCell();
	I_Id = D_Pack.ReadCell();
	I_Priority = D_Pack.ReadCell();
	I_Team = D_Pack.ReadCell();

	for (int i = 0; i < 3; i++) {
		F_Pos[i] = D_Pack.ReadFloat();
	}

	for (int i = 0; i < 3; i++) {
		F_Eye[i] = D_Pack.ReadFloat();
	}

	if (SQL_GetRowCount(H_Hndl) == 0) {
		Format(C_buffer, 512, "INSERT INTO ex_SpawnPoints VALUES ('%s', '%i', '%i', '%i', '%.3f', '%.3f', '%.3f', '%.3f', '%.3f', '%.3f');", C_MapName, I_Id, I_Priority, I_Team, F_Pos[0], F_Pos[1], F_Pos[2], F_Eye[0], F_Eye[1], F_Eye[2]);
	} else {
		Format(C_buffer, 512, "UPDATE ex_SpawnPoints SET priority = '%i', team = '%i', pos_x = '%.3f', pos_y = '%.3f', pos_z = '%.3f', eye_x = '%.3f', eye_y = '%.3f', eye_z = '%.3f' WHERE id = '%i' AND mapname = '%s'", I_Priority, I_Team, F_Pos[0], F_Pos[1], F_Pos[2], F_Eye[0], F_Eye[1], F_Eye[2], I_Id, C_MapName);
	}

	SQL_TQuery(gD_Main, sqlInsertSpawnCallback, C_buffer, I_UserId, DBPrio_Normal);
	CloseHandle(D_Pack);
}

public void sqlInsertSpawnCallback(Handle H_Owner, Handle H_Hndl, char[] C_Error, int I_UserId) {
	if (H_Hndl == null) {
		LogError("%s (InsertSpawn) - %s", SQL_PREFIX, C_Error);
		return;
	}

	sql_LoadSpawnPoints();

	int I_Client = GetClientOfUserId(I_UserId);

	if (IsValidClient(I_Client)) {
		if (gI_CurrentlyEditing[I_Client] != -1) {
			admin_IndepthViewSpawn(I_Client, gI_CurrentlyEditing[I_Client]);
			PrintToChat(I_Client, "%s Spawn Updated", PLUGIN_PREFIX);
		} else {
			PrintToChat(I_Client, "%s Spawn Inserted", PLUGIN_PREFIX);
			admin_SpawnManagement(I_Client);
		}
	}
}

public void sql_LoadItems() {
	char[] C_buffer = new char[512];

	Format(C_buffer, 512, "SELECT * FROM `ex_Items` WHERE mapname = '%s' ORDER BY id ASC", gC_MapName);
	SQL_TQuery(gD_Main, sqlLoadItemsCallback, C_buffer, _, DBPrio_High);
}

public void sqlLoadItemsCallback(Handle H_Owner, Handle H_Hndl, char[] C_Error, any A_Data) {
	if (H_Hndl == null) {
		LogError("%s (LoadItems) - %s", SQL_PREFIX, C_Error);
		return;
	}

	misc_ClearItems();

	while (SQL_FetchRow(H_Hndl)) {
		gA_Items.Resize(gI_Items + 1);

		gA_Items.Set(gI_Items, SQL_FetchInt(H_Hndl, 1), 0);
		gA_Items.Set(gI_Items, SQL_FetchInt(H_Hndl, 2), 1);

		for (int i = 0; i < 3; i++) {
			gA_Items.Set(gI_Items, SQL_FetchFloat(H_Hndl, i + 3), i + 2);
		}

		gI_Items++;
	}
}

public void sql_TryInsertItem(int I_Client, int I_Id, int I_ItemId, float F_Pos[3]) {
	char[] C_buffer = new char[512];
	int I_UserId;

	DataPack D_Pack = new DataPack();

	I_UserId = GetClientUserId(I_Client);

	D_Pack.WriteCell(I_UserId);
	D_Pack.WriteCell(I_Id);
	D_Pack.WriteCell(I_ItemId);

	for (int i = 0; i < 3; i++) {
		D_Pack.WriteCell(F_Pos[i]);
	}

	Format(C_buffer, 512, "SELECT * FROM `ex_Items` WHERE mapname = '%s' AND id = '%i'", gC_MapName, I_Id);
	SQL_TQuery(gD_Main, sqlTryInsertItemsCallback, C_buffer, D_Pack, DBPrio_Normal);
}

public void sqlTryInsertItemsCallback(Handle H_Owner, Handle H_Hndl, char[] C_Error, DataPack D_Pack) {
	if (H_Hndl == null) {
		LogError("%s (TryInsertItems) - %s", SQL_PREFIX, C_Error);
		return;
	}

	char[] C_buffer = new char[512];

	int I_UserId, I_Id, I_ItemId;
	float F_Pos[3];

	D_Pack.Reset();
	I_UserId = D_Pack.ReadCell();
	I_Id = D_Pack.ReadCell();
	I_ItemId = D_Pack.ReadCell();

	for (int i = 0; i < 3; i++) {
		F_Pos[i] = D_Pack.ReadCell();
	}

	if (SQL_GetRowCount(H_Hndl) == 0) {
		Format(C_buffer, 512, "INSERT INTO `ex_Items` VALUES ('%s', '%i', '%i', '%.3f', '%.3f', '%.3f')", gC_MapName, I_Id, I_ItemId, F_Pos[0], F_Pos[1], F_Pos[2]);
	} else {
		Format(C_buffer, 512, "UPDATE `ex_Items` SET itemid = '%i', pos_x = '%.3f', pos_y = '%.3f' AND pos_z = '%.3f' WHERE mapname = '%s' AND id = '%i'", I_ItemId, F_Pos[0], F_Pos[1], F_Pos[2], gC_MapName, I_Id);
	}

	SQL_TQuery(gD_Main, sqlInsertItemsCallback, C_buffer, I_UserId, DBPrio_Normal);
	CloseHandle(D_Pack);
}

public void sqlInsertItemsCallback(Handle H_Owner, Handle H_Hndl, char[] C_Error, int I_UserId) {
	if (H_Hndl == null) {
		LogError("%s (InsertItems) - %s", SQL_PREFIX, C_Error);
		return;
	}

	sql_LoadItems();

	int I_Client = GetClientOfUserId(I_UserId);

	if (IsValidClient(I_Client)) {
		PrintToChat(I_Client, "%s Item Inserted", PLUGIN_PREFIX);
	}
}
