public void admin_Menu(int I_Client) {
	admin_TryClearData(I_Client);
	gI_CurrentlyEditing[I_Client] = -1;

	char[] C_buffer = new char[512];
	Menu M_Menu = new Menu(AdminMenuHandle);

	Format(C_buffer, 512, "Executions - Admin\n");
	Format(C_buffer, 512, "%s \n", C_buffer);
	M_Menu.SetTitle(C_buffer);

	Format(C_buffer, 512, "Config Management\n");
	Format(C_buffer, 512, "%s > PLACEHOLDER\n", C_buffer);
	Format(C_buffer, 512, "%s \n", C_buffer);
	M_Menu.AddItem(C_buffer, C_buffer, ITEMDRAW_DISABLED);

	Format(C_buffer, 512, "Spawn Management\n");
	Format(C_buffer, 512, "%s > PLACEHOLDER\n", C_buffer);
	Format(C_buffer, 512, "%s \n", C_buffer);
	M_Menu.AddItem(C_buffer, C_buffer);

	Format(C_buffer, 512, "Item Management\n");
	Format(C_buffer, 512, "%s > PLACEHOLDER\n", C_buffer);
	Format(C_buffer, 512, "%s \n", C_buffer);
	M_Menu.AddItem(C_buffer, C_buffer);

	M_Menu.Display(I_Client, 0);
}

public int AdminMenuHandle(Menu M_Menu, MenuAction mA_Action, int I_Param1, int I_Param2) {
	if (mA_Action == MenuAction_Select) {
		switch (I_Param2) {
			case 0: {
				//admin_ConfigManagement(I_Param1);
			} case 1: {
				admin_SpawnManagement(I_Param1);
			} case 2: {
				admin_ItemManagement(I_Param1);
			}
		}
	}
}

public void admin_SpawnManagement(int I_Client) {
	char[] C_buffer = new char[512];
	Menu M_Menu = new Menu(AdminSpawnManagementHandle);

	Format(C_buffer, 512, "Executions - Admin - Spawn Management\n");
	Format(C_buffer, 512, "%s \n", C_buffer);
	M_Menu.SetTitle(C_buffer);

	Format(C_buffer, 512, "View Spawns\n");
	Format(C_buffer, 512, "%s > PLACEHOLDER\n", C_buffer);
	Format(C_buffer, 512, "%s \n", C_buffer);
	M_Menu.AddItem(C_buffer, C_buffer);

	Format(C_buffer, 512, "Add Spawns\n");
	Format(C_buffer, 512, "%s > PLACEHOLDER\n", C_buffer);
	Format(C_buffer, 512, "%s \n", C_buffer);
	M_Menu.AddItem(C_buffer, C_buffer);

	Format(C_buffer, 512, "PLACEHOLDER\n");
	Format(C_buffer, 512, "%s > PLACEHOLDER\n", C_buffer);
	Format(C_buffer, 512, "%s \n", C_buffer);
	M_Menu.AddItem(C_buffer, C_buffer, ITEMDRAW_DISABLED);

	M_Menu.ExitBackButton = true;
	M_Menu.Display(I_Client, 0);
}

public int AdminSpawnManagementHandle(Menu M_Menu, MenuAction mA_Action, int I_Param1, int I_Param2) {
	if (I_Param2 == MenuCancel_ExitBack) {
		admin_Menu(I_Param1);
		return;
	}

	if (mA_Action == MenuAction_Select) {
		gI_CurrentlyEditing[I_Param1] = -1;
		switch (I_Param2) {
			case 0: {
				admin_ViewSpawns(I_Param1);
			} case 1: {
				admin_AddObjectTimer(I_Param1, 0);
			} case 2: {

			}
		}
	}
}

public void admin_ViewSpawns(int I_Client) {
	char[] C_buffer = new char[512];
	Menu M_Menu = new Menu(AdminViewSpawnsHandle);

	Format(C_buffer, 512, "Executions - Admin - Spawn Management - View Spawns\n");
	Format(C_buffer, 512, "%s \n", C_buffer);
	Format(C_buffer, 512, "%sCurrent Saved Spawn Points: %i\n", C_buffer, gI_SpawnPoints[0] + gI_SpawnPoints[1]);
	Format(C_buffer, 512, "%s \n", C_buffer);
	M_Menu.SetTitle(C_buffer);

	int I_Length = gA_SpawnPoints.Length;

	if (I_Length == 0) {
		Format(C_buffer, 512, "No Spawn Points\n");
		M_Menu.AddItem(C_buffer, C_buffer, ITEMDRAW_DISABLED);
	} else {
		for (int i = 0; i < I_Length; i++) {
			Format(C_buffer, 512, "ID: %i\n", gA_SpawnPoints.Get(i, 0));
			Format(C_buffer, 512, "%s > Team: %i, Prority: %i", C_buffer, gA_SpawnPoints.Get(i, 2), gA_SpawnPoints.Get(i, 1));

			M_Menu.AddItem(C_buffer, C_buffer);
		}
	}


	M_Menu.ExitBackButton = true;
	M_Menu.Display(I_Client, 0);
}

public int AdminViewSpawnsHandle(Menu M_Menu, MenuAction mA_Action, int I_Param1, int I_Param2) {
	if (I_Param2 == MenuCancel_ExitBack) {
		admin_SpawnManagement(I_Param1);
		return;
	}

	if (mA_Action == MenuAction_Select) {
		gI_CurrentlyEditing[I_Param1] = I_Param2;

		admin_IndepthViewSpawn(I_Param1, I_Param2);
	}
}

public void admin_AddObjectTimer(int I_Client, int I_Type) {
	gI_CurrentType[I_Client] = I_Type;

	gH_AddSpawns[I_Client] = CreateTimer(0.01, Timer_AddObjects, I_Client, TIMER_REPEAT);
}

public Action Timer_AddObjects(Handle H_Timer, int I_Client) {
	admin_AddObject(I_Client, gI_CurrentType[I_Client]);
}

public void admin_AddObject(int I_Client, int I_Type) {
	char[] C_buffer = new char[512];
	Menu M_Menu = new Menu(AdminAddObjectsHandle);

	float F_Pos[3], F_Eye[3];

	GetClientAbsOrigin(I_Client, F_Pos);
	GetClientEyeAngles(I_Client, F_Eye);

	if (I_Type == 0) {
		if (gI_CurrentlyEditing[I_Client] != -1) {
			Format(C_buffer, 512, "Executions - Admin - Spawn Management - Edit\n");
		} else {
			Format(C_buffer, 512, "Executions - Admin - Spawn Management - Add\n");
		}

		Format(C_buffer, 512, "%s \n", C_buffer);
		Format(C_buffer, 512, "%sPositional Values\n", C_buffer);
		Format(C_buffer, 512, "%s> X - %.2f, Y - %.2f, Z - %.2f\n", C_buffer, F_Pos[0], F_Pos[1], F_Pos[2]);
		Format(C_buffer, 512, "%s \n", C_buffer);
		Format(C_buffer, 512, "%sEye Values\n", C_buffer);
		Format(C_buffer, 512, "%s> X - %.2f, Y - %.2f, Z - %.2f\n", C_buffer, F_Eye[0], F_Eye[1], F_Eye[2]);
		Format(C_buffer, 512, "%s \n", C_buffer);
		M_Menu.SetTitle(C_buffer);

		Format(C_buffer, 512, "Save Spawn");
		M_Menu.AddItem(C_buffer, C_buffer);
	} else {
		if (gI_CurrentlyEditing[I_Client] != -1) {
			Format(C_buffer, 512, "Executions - Admin - Item Management - Edit\n");
		} else {
			Format(C_buffer, 512, "Executions - Admin - Item Management - Add\n");
		}

		Format(C_buffer, 512, "%s \n");
		Format(C_buffer, 512, "%sPositional Values\n", C_buffer);
		Format(C_buffer, 512, "%s> X - %.2f, Y - %.2f, Z - %.2f\n", C_buffer, F_Pos[0], F_Pos[1], F_Pos[2]);
		Format(C_buffer, 512, "%s \n", C_buffer);
		M_Menu.SetTitle(C_buffer);

		Format(C_buffer, 512, "Save Item");
		M_Menu.AddItem(C_buffer, C_buffer);
	}

	M_Menu.ExitBackButton = true;
	M_Menu.Display(I_Client, 0);
}

public int AdminAddObjectsHandle(Menu M_Menu, MenuAction mA_Action, int I_Param1, int I_Param2) {
	int I_Edit;

	if (IsValidClient(I_Param1)) {
		I_Edit = gI_CurrentlyEditing[I_Param1];
	}

	if ((mA_Action == MenuAction_Cancel) && (I_Param2 != MenuCancel_Interrupted)) {
		admin_TryClearData(I_Param1);
	}

	if (I_Param2 == MenuCancel_ExitBack) {
		if (gI_CurrentType[I_Param1] == 0) {
			if (I_Edit != -1) {
				admin_IndepthViewSpawn(I_Param1, I_Edit);
			} else {
				admin_SpawnManagement(I_Param1);
			}
		} else {
			if (I_Edit != -1) {

			} else {

			}
		}

		return;
	}

	if (mA_Action == MenuAction_Select) {
		admin_TryClearData(I_Param1);

		GetClientAbsOrigin(I_Param1, gF_SaveData[I_Param1][0]);
		GetClientEyeAngles(I_Param1, gF_SaveData[I_Param1][1]);

		if (gI_CurrentType[I_Param1] == 0) {
			if (I_Edit != -1) {
				sql_TryInsertSpawn(I_Param1, gA_SpawnPoints.Get(I_Edit, 0), gA_SpawnPoints.Get(I_Edit, 1), gA_SpawnPoints.Get(I_Edit, 2), gF_SaveData[I_Param1][0], gF_SaveData[I_Param1][1]);
			} else {
				admin_SaveSpawn(I_Param1, 0);
			}
		} else {
			if (I_Edit != -1) {
				sql_TryInsertItem(I_Param1, gA_Items.Get(I_Edit, 0), gA_Items.Get(I_Edit, 1), gF_SaveData[I_Param1][0]);
			} else {
				admin_SaveItem(I_Param1, 0);
			}
		}
	}
}

public void admin_IndepthViewSpawn(int I_Client, int I_Index) {
	char[] C_buffer = new char[512];
	char[] C_Team = new char[8];
	Menu M_Menu = new Menu(AdminIndepthViewSpawnHandle);

	misc_FormatTeam(gA_SpawnPoints.Get(I_Index, 2), C_Team, 8);

	Format(C_buffer, 512, "Executions - Admin - Spawn Management - View Spawns - Info\n");
	Format(C_buffer, 512, "%s \n", C_buffer);
	Format(C_buffer, 512, "%sSelected ID: %i\n", C_buffer, gA_SpawnPoints.Get(I_Index, 0));
	Format(C_buffer, 512, "%s \n", C_buffer);
	Format(C_buffer, 512, "%sPriority: %i\n", C_buffer, gA_SpawnPoints.Get(I_Index, 1));
	Format(C_buffer, 512, "%sTeam: %s\n", C_buffer, C_Team);
	Format(C_buffer, 512, "%s \n", C_buffer);
	M_Menu.SetTitle(C_buffer);

	Format(C_buffer, 512, "Teleport To Spawn\n", C_buffer);
	Format(C_buffer, 512, "%s \n", C_buffer);
	M_Menu.AddItem(C_buffer, C_buffer);

	Format(C_buffer, 512, "Edit Zone");
	M_Menu.AddItem(C_buffer, C_buffer);

	Format(C_buffer, 512, "Edit Values");
	M_Menu.AddItem(C_buffer, C_buffer);

	M_Menu.ExitBackButton = true;
	M_Menu.Display(I_Client, 0);
}

public int AdminIndepthViewSpawnHandle(Menu M_Menu, MenuAction mA_Action, int I_Param1, int I_Param2) {
	if (I_Param2 == MenuCancel_ExitBack) {
		admin_ViewSpawns(I_Param1);

		return;
	}

	if (mA_Action == MenuAction_Select) {
		switch (I_Param2) {
			case 0: {
				float F_Pos[3], F_Eye[3];

				for (int i = 0; i < 3; i++) {
					F_Pos[i] = gA_SpawnPoints.Get(gI_CurrentlyEditing[I_Param1], i + 3);
					F_Eye[i] = gA_SpawnPoints.Get(gI_CurrentlyEditing[I_Param1], i + 6);
				}

				F_Eye[2] = 0.0;

				TeleportEntity(I_Param1, F_Pos, F_Eye, view_as<float>({0.0, 0.0, 0.0}));
				admin_IndepthViewSpawn(I_Param1, gI_CurrentlyEditing[I_Param1]);
			} case 1: {
				admin_AddObjectTimer(I_Param1, 0);
			} case 2: {
				admin_EditValues(I_Param1, I_Param2);
			}
		}
	}
}

public void admin_EditValues(int I_Client, int I_Index) {
	char[] C_buffer = new char[512];
	Menu menu = new Menu(AdminEditValuesHandle);

	Format(C_buffer, 512, "Executions - Admin - Spawn Management - View Spawns - Edit Values\n");
	Format(C_buffer, 512, "%s \n", C_buffer);
	menu.SetTitle(C_buffer);

	Format(C_buffer, 512, "PLACEHOLDER");
	menu.AddItem(C_buffer, C_buffer);

	menu.Display(I_Client, 0);
}

public int AdminEditValuesHandle(Menu M_Menu, MenuAction mA_Action, int I_Param1, int I_Param2) {
}

public void admin_SaveSpawn(int I_Client, int I_Selection) {
	char[] C_buffer = new char[512];
	char[] C_Team = new char[8];
	Menu M_Menu = new Menu(AdminSaveSpawnHandle);

	if (I_Selection < 6) {
		I_Selection = 0;
	} else {
		I_Selection = 6;
	}

	Format(C_buffer, 512, "Executions - Admin - Spawn Management - Save\n");
	Format(C_buffer, 512, "%s \n", C_buffer);
	Format(C_buffer, 512, "%sPositional Values\n", C_buffer);
	Format(C_buffer, 512, "%s> X - %.2f, Y - %.2f, Z - %.2f\n", C_buffer, gF_SaveData[I_Client][0][0], gF_SaveData[I_Client][0][1], gF_SaveData[I_Client][0][2]);
	Format(C_buffer, 512, "%s \n", C_buffer);
	Format(C_buffer, 512, "%sEye Values\n", C_buffer);
	Format(C_buffer, 512, "%s> X - %.2f, Y - %.2f, Z - %.2f\n", C_buffer, gF_SaveData[I_Client][1][0], gF_SaveData[I_Client][1][1], gF_SaveData[I_Client][1][2]);
	Format(C_buffer, 512, "%s \n", C_buffer);
	M_Menu.SetTitle(C_buffer);

	Format(C_buffer, 512, "Edit Spawn\n");
	M_Menu.AddItem(C_buffer, C_buffer);

	Format(C_buffer, 512, "Teleport To Spawn\n");
	M_Menu.AddItem(C_buffer, C_buffer);

	Format(C_buffer, 512, "Save Spawn\n");
	Format(C_buffer, 512, "%s \n", C_buffer);
	M_Menu.AddItem(C_buffer, C_buffer);

	Format(C_buffer, 512, "Priority: [%i]", gI_SaveSettings[I_Client][0]);
	M_Menu.AddItem(C_buffer, C_buffer);

	misc_FormatTeam(gI_SaveSettings[I_Client][1] + 2, C_Team, 32);
	Format(C_buffer, 512, "Team: [%s]", C_Team);
	M_Menu.AddItem(C_buffer, C_buffer);

	M_Menu.ExitBackButton = true;
	M_Menu.DisplayAt(I_Client, I_Selection, 0);
}

public int AdminSaveSpawnHandle(Menu M_Menu, MenuAction mA_Action, int I_Param1, int I_Param2) {
	bool B_Show = true;

	if ((mA_Action == MenuAction_Cancel) && (I_Param2 != MenuCancel_Interrupted)) {
		B_Show = false;

		admin_TryClearData(I_Param1);
		gI_CurrentlyEditing[I_Param1] = -1;
	}

	if (I_Param2 == MenuCancel_ExitBack) {
		B_Show = false;

		admin_SpawnManagement(I_Param1);
		return;
	}

	if (mA_Action == MenuAction_Select) {
		switch (I_Param2) {
			case 0: {
				admin_AddObjectTimer(I_Param1, 0);
			} case 1: {
				gF_SaveData[I_Param1][1][2] = 0.0;
				TeleportEntity(I_Param1, gF_SaveData[I_Param1][0], gF_SaveData[I_Param1][1], view_as<float>({0.0, 0.0, 0.0}));
			} case 2: {
				B_Show = false;

				sql_TryInsertSpawn(I_Param1, gI_SpawnPoints[0] + gI_SpawnPoints[1], gI_SaveSettings[I_Param1][0], gI_SaveSettings[I_Param1][1] + 2, gF_SaveData[I_Param1][0], gF_SaveData[I_Param1][1]);
			} case 3: {
				if (gI_SaveSettings[I_Param1][0] < 5) {
					gI_SaveSettings[I_Param1][0]++;
				} else {
					gI_SaveSettings[I_Param1][0] = 1;
				}

				PrintToChat(I_Param1, "%s Priority: \x07%i", PLUGIN_PREFIX, gI_SaveSettings[I_Param1][0]);
			} case 4: {
				char[] C_Team = new char[8];

				if (gI_SaveSettings[I_Param1][1] < 1) {
					gI_SaveSettings[I_Param1][1]++;
				} else {
					gI_SaveSettings[I_Param1][1] = 0;
				}

				misc_FormatTeam(gI_SaveSettings[I_Param1][1] + 2, C_Team, 8);
				PrintToChat(I_Param1, "%s Team: \x07%s", PLUGIN_PREFIX, C_Team);
			} case 5: {

			}
		}

		if (B_Show) {
			admin_SaveSpawn(I_Param1, I_Param2);
		}
	}
}

public void admin_ItemManagement(int I_Client) {
	char[] C_buffer = new char[512];
	Menu M_Menu = new Menu(AdminItemManagementHandle);

	Format(C_buffer, 512, "Executions - Admin - Item Management\n");
	Format(C_buffer, 512, "%s \n", C_buffer);
	M_Menu.SetTitle(C_buffer);

	Format(C_buffer, 512, "View Items\n");
	Format(C_buffer, 512, "%s > PLACEHOLDER\n", C_buffer);
	Format(C_buffer, 512, "%s \n", C_buffer);
	M_Menu.AddItem(C_buffer, C_buffer);

	Format(C_buffer, 512, "Add Items\n");
	Format(C_buffer, 512, "%s > PLACEHOLDER\n", C_buffer);
	Format(C_buffer, 512, "%s \n", C_buffer);
	M_Menu.AddItem(C_buffer, C_buffer);

	Format(C_buffer, 512, "PLACEHOLDER\n");
	Format(C_buffer, 512, "%s > PLACEHOLDER\n", C_buffer);
	Format(C_buffer, 512, "%s \n", C_buffer);
	M_Menu.AddItem(C_buffer, C_buffer, ITEMDRAW_DISABLED);

	M_Menu.ExitBackButton = true;
	M_Menu.Display(I_Client, 0);
}

public int AdminItemManagementHandle(Menu M_Menu, MenuAction mA_Action, int I_Param1, int I_Param2) {
	if (I_Param2 == MenuCancel_ExitBack) {
		admin_Menu(I_Param1);
		return;
	}

	if (mA_Action == MenuAction_Select) {
		switch (I_Param2) {
			case 0: {
				admin_ViewItems(I_Param1);
			} case 1: {
				admin_AddObjectTimer(I_Param1, 1);
			} case 2: {

			}
		}
	}
}

public void admin_ViewItems(int I_Client) {
	char[] C_buffer = new char[512];
	Menu M_Menu = new Menu(AdminViewItemsHandle);

	Format(C_buffer, 512, "Executions - Admin - Item Management - View Items\n");
	Format(C_buffer, 512, "%s \n", C_buffer);
	Format(C_buffer, 512, "%sCurrent Saved Items: %i\n", C_buffer, gI_Items);
	Format(C_buffer, 512, "%s \n", C_buffer);
	M_Menu.SetTitle(C_buffer);

	int I_Length = gA_Items.Length;

	if (I_Length == 0) {
		Format(C_buffer, 512, "No Items\n");
		M_Menu.AddItem(C_buffer, C_buffer, ITEMDRAW_DISABLED);
	} else {
		for (int i = 0; i < I_Length; i++) {
			Format(C_buffer, 512, "ID: %i\n", gA_Items.Get(i, 0));

			M_Menu.AddItem(C_buffer, C_buffer);
		}
	}


	M_Menu.ExitBackButton = true;
	M_Menu.Display(I_Client, 0);
}

public int AdminViewItemsHandle(Menu M_Menu, MenuAction mA_Action, int I_Param1, int I_Param2) {
	if (I_Param2 == MenuCancel_ExitBack) {
		admin_ItemManagement(I_Param1);
		return;
	}

	if (mA_Action == MenuAction_Select) {
		admin_IndepthViewItem(I_Param1, I_Param2);
	}
}

public void admin_SaveItem(int I_Client, int I_Selection) {
	char[] C_buffer = new char[512];
	char[] C_Item = new char[16];
	Menu M_Menu = new Menu(AdminSaveItemHandle);

	if (I_Selection < 6) {
		I_Selection = 0;
	} else {
		I_Selection = 6;
	}

	Format(C_buffer, 512, "Executions - Admin - Item Management - Save\n");
	Format(C_buffer, 512, "%s \n", C_buffer);
	Format(C_buffer, 512, "%sPositional Values\n", C_buffer);
	Format(C_buffer, 512, "%s> X - %.2f, Y - %.2f, Z - %.2f\n", C_buffer, gF_SaveData[I_Client][0][0], gF_SaveData[I_Client][0][1], gF_SaveData[I_Client][0][2]);
	Format(C_buffer, 512, "%s \n", C_buffer);
	M_Menu.SetTitle(C_buffer);

	Format(C_buffer, 512, "Edit Item\n");
	M_Menu.AddItem(C_buffer, C_buffer);

	Format(C_buffer, 512, "Teleport To Item\n");
	M_Menu.AddItem(C_buffer, C_buffer);

	Format(C_buffer, 512, "Save Item\n");
	Format(C_buffer, 512, "%s \n", C_buffer);
	M_Menu.AddItem(C_buffer, C_buffer);

	misc_FormatItem(gI_SaveSettings[I_Client][0], C_Item, 16)
	Format(C_buffer, 512, "Item: [%s]", C_Item);
	M_Menu.AddItem(C_buffer, C_buffer);

	M_Menu.ExitBackButton = true;
	M_Menu.DisplayAt(I_Client, I_Selection, 0);
}

public int AdminSaveItemHandle(Menu M_Menu, MenuAction mA_Action, int I_Param1, int I_Param2) {
	bool B_Show = true;

	if ((mA_Action == MenuAction_Cancel) && (I_Param2 != MenuCancel_Interrupted)) {
		B_Show = false;

		admin_TryClearData(I_Param1);
		gI_CurrentlyEditing[I_Param1] = -1;
	}

	if (I_Param2 == MenuCancel_ExitBack) {
		B_Show = false;

		admin_ItemManagement(I_Param1);
		return;
	}

	if (mA_Action == MenuAction_Select) {
		switch (I_Param2) {
			case 0: {
				admin_AddObjectTimer(I_Param1, 1);
			} case 1: {
				TeleportEntity(I_Param1, gF_SaveData[I_Param1][0], NULL_VECTOR, view_as<float>({0.0, 0.0, 0.0}));
			} case 2: {
				B_Show = false;

				sql_TryInsertItem(I_Param1, gI_Items, gI_SaveSettings[I_Param1][0], gF_SaveData[I_Param1][0]);
			} case 3: {
				char[] C_Item = new char[16];

				if (gI_SaveSettings[I_Param1][0] < 1) {
					gI_SaveSettings[I_Param1][0]++;
				} else {
					gI_SaveSettings[I_Param1][0] = 0;
				}

				misc_FormatItem(gI_SaveSettings[I_Param1][0], C_Item, 16);
				PrintToChat(I_Param1, "%s Item: \x07%s", PLUGIN_PREFIX, C_Item);
			}
		}

		if (B_Show) {
			admin_SaveItem(I_Param1, I_Param2);
		}
	}
}

public void admin_IndepthViewItem(int I_Client, int I_Index) {
	char[] C_buffer = new char[512];

	Menu M_Menu = new Menu(AdminIndepthViewItemHandle);
}

public int AdminIndepthViewItemHandle(Menu M_Menu, MenuAction mA_Action, int I_Param1, int I_Param2) {


}

public void admin_TryClearData(int I_Client) {
	for (int i = 0; i < 2; i++) {
		for (int x = 0; x < 3; x++) {
			gF_SaveData[I_Client][i][x] = 0.0;
		}
	}

	gI_SaveSettings[I_Client][0] = 1;
	gI_CurrentType[I_Client] = 0;

	for (int i = 1; i < 2; i++) {
		gI_SaveSettings[I_Client][i] = 0;
	}

	if (gH_AddSpawns[I_Client] != INVALID_HANDLE) {
		CloseHandle(gH_AddSpawns[I_Client]);
		gH_AddSpawns[I_Client] = INVALID_HANDLE;
	}
}
