/* PLUGIN */
Database gD_Main;

/* MAP */
ArrayList gA_SpawnPoints;
ArrayList gA_Items;

int gI_SpawnPoints[2];
int gI_Items;

char gC_MapName[128];

/* ADMIN */
Handle gH_AddSpawns[MAXPLAYERS + 1];

int gI_SaveSettings[MAXPLAYERS + 1][2];
int gI_CurrentlyEditing[MAXPLAYERS + 1];
int gI_CurrentType[MAXPLAYERS + 1];

float gF_SaveData[MAXPLAYERS + 1][2][3];
