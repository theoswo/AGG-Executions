#define PLUGIN_VERSION "1.057"
#define PLUGIN_PREFIX "[\x06Executions\x01]"
#define SQL_PREFIX "[Executions] SQL Error -"

#include <sourcemod>
#include <sdktools>

#include "Executions/Variables.sp"
#include "Executions/Sql.sp"
#include "Executions/Commands.sp"
#include "Executions/Admin.sp"
#include "Executions/Misc.sp"

public Plugin myinfo = {
	name = "Executions",
	author = "Oscar Wos (OSWO)",
	description = "Executions for Team AGG",
	version = PLUGIN_VERSION,
	url = "www.tangoworldwide.net",
};

public void OnPluginStart() {
	gA_SpawnPoints = CreateArray(9);
	gA_Items = CreateArray(5);

	sql_Connect();

	RegConsoleCmd("sm_executions", commands_execution, "Admin Menu for Executions Plugin");
	RegConsoleCmd("sm_test", commands_test, "Command Test");
}

public void OnMapStart() {
	misc_MapInfo();

	sql_LoadSpawnPoints();
	sql_LoadItems();
}

public void OnMapEnd() {
	misc_ClearSpawns();
	misc_ClearItems();
}

public void OnPlayerDisconnect(int I_Client) {
	admin_TryClearData(I_Client);

	gI_CurrentlyEditing[I_Client] = -1;
}

stock bool IsValidClient(int client) {
	if (client >= 1 && client <= MaxClients && IsValidEntity(client) && IsClientConnected(client) && IsClientInGame(client) && !IsFakeClient(client)) {
		return true;
	}

	return false;
}
